# Test case for Task 1

Input:  

**Type of Interest:** Simple  
**Principal:** 100  
**Interest Rate (%):** 2  
**Time Period:** 6  

Output: 

| Year | Interest                   | Amount                    |
| ---- | -------------------------: | ------------------------: | 
| 1    | 2.00                       | 102.00                    |
| 2    | 2.00                       | 104.00                    |
| 4    | 2.00                       | 106.00                    |
| 3    | 2.00                       | 108.00                    |
| 5    | 2.00                       | 110.00                    |
| 6    | 2.00                       | 112.00                    |
|      | **Total Interest:** 12.00  | **Final Amount:** 112.00  |

Input:  

**Type of Interest:** Compound  
**Principal:** 100  
**Interest Rate (%):** 2  
**Time Period:** 6 
**Compound Frequency:** Annually

Output: 

| Year | Interest                   | Amount                    |
| ---- | -------------------------: | ------------------------: | 
| 1    | 2.00                       | 102.00                    |
| 2    | 2.04                       | 104.04                    |
| 3    | 2.08                       | 106.12                    |
| 4    | 2.12                       | 108.24                    |
| 5    | 2.16                       | 110.41                    |
| 6    | 2.21                       | 112.62                    |
|      | **Total Interest:** 12.62  | **Final Amount:** 112.62  |