var app = angular.module("InterestCalculator", []);

app.controller("CalculatorController", ['$scope', function ($scope) {
    // DEFAULT VALUES FOR INPUT
    $scope.input = {
        interestType: "2",
        principal: 100,
        interestRate: 2,
        timePeriod: 6,
        compoundFrequency: "1"
    }
    $scope.errors = {};
    $scope.output = false;

    $scope.validate = function () {
        console.log('validate here')
        return true;
    }

    $scope.calculate = function () {
        $scope.output = false;
        if ($scope.validate() == false) return false;

        switch ($scope.input.interestType) {
            case "1":  // Simple
                $scope.output = calculateSimple($scope.input);
                break;
            case "2":  // Compund
                $scope.output = calculateCompund($scope.input);
                break;
        }
    }

    $scope.$watch('input', function() {
        $scope.output = false;
    }, true);

    function calculateSimple(args) {
        var output = {
            details: [],
            summary: {
                interest: 0,
                balance: 0
            }
        };
        var balance = args.principal;
        for (i = 1; i <= args.timePeriod; i++) {
            var interest = args.principal * (args.interestRate / 100);
            balance += interest;
            output.details.push({
                year: i,
                interest: interest,
                balance: balance
            });
        }
        output.summary.interest = balance - args.principal;
        output.summary.balance = balance;
        return output;
    }

    function calculateCompund(args) {
        var output = {
            details: [],
            summary: {
                interest: 0,
                balance: 0
            }
        };
        var principal = args.principal;
        var compoundFrequency = args.compoundFrequency;
        var periodicInterestRate = (args.interestRate / compoundFrequency) * 0.01;

        for (i = 1; i <= args.timePeriod; i++) {
            var interest = 0;
            for (j = 0; j < compoundFrequency; j++) {
                interest += principal * periodicInterestRate
                principal += principal * periodicInterestRate;
            }
            output.details.push({
                year: i,
                interest: interest,
                balance: principal
            });
        }

        output.summary.interest = principal - args.principal;
        output.summary.balance = principal;        
        return output;
    }

}]);